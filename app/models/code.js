var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validate = require('mongoose-validator');

/*
* mongoose-validator middleware
* https://github.com/leepowellcouk/mongoose-validator
*/
var codeValidator = [
  validate({
    validator: 'isLength',
    arguments: [36, 36],
    message: 'Profile code should be exactly {ARGS[0]} characters long.'
  })
];

var CodeSchema = new Schema({
  code: { type: String, required: true, unique: true, validate: codeValidator },
  notes: { type: String },
  game: { type: String },
  kb_model: { type: String, required: true },
  created_at: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.model('Code', CodeSchema);
