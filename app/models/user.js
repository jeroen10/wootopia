var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var validate = require('mongoose-validator');

/*
* mongoose-validator middleware
* https://github.com/leepowellcouk/mongoose-validator
*/
var usernameValidator = [
  validate({
    validator: 'isLength',
    arguments: [4, 64],
    message: 'Username must be between {ARGS[0]} and {ARGS[1]} characters'
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'Username can only contain letters and numbers'
  })
];

var emailValidator = [
  validate({
    validator: 'isEmail',
    message: 'Please provide a valid email adress'
  }),
  validate({
    validator: 'isLength',
    arguments: [5, 254],
    message: 'Email must be between {ARGS[0]} and {ARGS[1]} characters'
  })
];

var passwordValidator = [
  validate({
    validator: 'isLength',
    arguments: [8, 254],
    message: 'Password must be between {ARGS[0]} and {ARGS[1]} characters'
  })
];

var UserSchema = new Schema({
  username: { type: String, required: true, unique: true, validate: usernameValidator },
  password: { type: String, required: true, validate: passwordValidator },
  email: { type: String, required: true, lowercase: true, unique: true, validate: emailValidator },
  codes: [{
    code: String,
  }],
  admin: { type: Boolean },
  created_at: { type: Date, required: true, default: Date.now }
});

UserSchema.pre('save', function(next) {
  var user = this;
  bcrypt.hash(user.password, null, null, function(err, hash) {
    if (err) {
      console.log(err);
      return next(err);
    }
    user.password = hash;
    next();
  });
});

UserSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
