var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validate = require('mongoose-validator');

var GameSchema = new Schema({
  igdb_id: { type: String, required: true, unique: true },
  name: { type: String, required: true, unique: true },
  codes: [{
    code: { type: String, required: true },
    published: { type: Boolean, required: true },
    notes: { type: String },
    rgb: { type: Object, required: true },
    created_at: { type: Date, required: true, default: Date.now }
  }],
  hasPublishedCode: { type: Boolean },
  cover: { type: Object },
  screenshots: { type: Object }
});

module.exports = mongoose.model('Game', GameSchema);
