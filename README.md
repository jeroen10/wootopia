# Wootopia
## A place to find and share profile codes for the Wooting One keyboard.

global npm packages:
- nodemons


### Environment variables
| Variable               | Description                     |
| ---------------------- | ------------------------------- |
| ```IGDB_USER_KEY```    | |
| ```SENDGRID_API_KEY``` | |
| ```ENV_TYPE```         | |




### API Endpoints
All endpoints are prefixed with ```/api```.


| Method      | Endpoint         | Usage                                      |
| ----------- | ---------------- | ------------------------------------------ |
| ```POST```  | /users           |   |
| ```GET```   | /users           | Returns a list of games as a JSON object. |
| ```POST```  | /updateusercodes |    $1 |
| ```POST```  | /codes           |    $1 |
| ```GET```   | /codes           |    $1 |
| ```POST```  | /games           |    $1 |
| ```GET```   | /games           |    $1 |
| ```GET```   | /games/:idgb_id  |    $1 |
| ```GET```   | /games/:idgb_id/:code  |    $1 |
| ```POST```  | /addcodetogame   |    $1 |
| ```POST```  | /updategamecode  |    $1 |
| ```GET```   | /checkforgame/:igdb_id  |    $1 |
| ```POST```  | /authenticate    |    $1 |
| ```GET```   | /queryigdb/:query |    $1 |
| ```GET```   | /getgamedetails/:igdb_id |    $1 |
| ```POST```   | /sendmailtoadmin |    $1 |
| ```POST```   | /me             |    $1 |
