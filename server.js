// Importing packages
require('dotenv').config();
var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var mongodb_uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/woot';
var router = express.Router();
var appRoutes = require('./app/routes/api')(router);
var path = require('path');
var passport = require('passport');
var social = require('./app/passport/passport')(app, passport);

// Initializing middleware
app.use(morgan('dev'));
app.use(bodyParser.json()); //for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express.static(__dirname + '/public'));
app.use('/api', appRoutes);

// Init MongoDB
mongoose.connect(mongodb_uri, { useMongoClient: true }, function(err ) {
  if (err) {
    console.log('Not connected to MongoDB: ' + err);
  } else {
    console.log('Now connected to MongoDB')
  }
});

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

app.listen(port, function() {
  console.log('Party started at port ' + port);
});
