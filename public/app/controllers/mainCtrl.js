angular.module('mainController', ['authServices'])

.controller('mainCtrl',  function(Auth, $timeout, $location, $rootScope, $window) {
  var app = this;

  app.loadme = false;

  $rootScope.$on('$routeChangeStart', function() {

    if (Auth.isLoggedIn()) {
      app.isLoggedIn = true;
      Auth.getUser().then(function(data) {
        app.userdata = data.data;
        app.isAdmin = app.userdata.admin;
        app.loadme = true;
      });
    } else {
      app.isLoggedIn = false;
      app.user = false;
      app.loadme = true;
    }

    if ($location.hash() == '_=_') $location.hash(null);
  });

  this.facebook = function() {
    $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/facebook';
  };

  this.twitter = function() {
    $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/twitter';
  };

  this.google = function() {
    $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/google';
  };

  this.doLogin = function (loginData) {
    app.loading = true;
    app.errMsg = false;

    Auth.login(app.loginData).then(function(data) {
      app.loading = false;
      if (data.data.success == true) {
        // app.successMsg = data.data.message + ' ....Redirecting';
        $timeout(function(){
          $location.path('/admin/dashboard');
          app.loginData = '';
          // app.succesMsg = false;
        }, 500)
      } else {
        app.errMsg = data.data.message;
      }
    });
  };

  this.logout = function() {
    Auth.logout();
    $location.path('/logout');
    $location.path('/');
  };

  this.isCurrentPath = function (path) {
    return $location.path() == path;
  };

});
