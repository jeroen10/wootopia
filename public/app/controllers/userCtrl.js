angular.module('userControllers', ['userServices'])

.controller('regCtrl', function($http, $location, $timeout, User) {

  var app = this;

  this.regUser = function (regData, valid) {
    app.loading = true;
    app.errMsg = false;

    if (valid) {
      User.create(app.regData).then(function(data) {
        app.loading = false;
        if (data.data.success == true) {
          app.successMsg = data.data.message + ' ....Redirecting';
          $timeout(function(){
            $location.path('/');
          }, 2000)
        } else {
          app.errMsg = data.data.message;
        }
      });
    } else {
      app.loading = false;
      app.errMsg = 'Please ensure you filled out the registration form correctly.';
    }
  }

})

.controller('facebookCtrl', function($routeParams, Auth, $location, $window) {

  var app = this;

  if ($window.location.pathname == '/facebookerror') {
    app.errMsg = "Facebook email was not found in Wooting Profiles database.";
  } else {
    console.log($routeParams.token);
    Auth.social($routeParams.token);
    $location.path('/');
  }

})

.controller('twitterCtrl', function($routeParams, Auth, $location, $window) {

  var app = this;

  if ($window.location.pathname == '/twittererror') {
    app.errMsg = "Twitter email was not found in Wooting Profiles database.";
  } else {
    Auth.social($routeParams.token);
    $location.path('/');
  }

})

.controller('googleCtrl', function($routeParams, Auth, $location, $window) {

  var app = this;

  if ($window.location.pathname == '/googleerror') {
    app.errMsg = "Google email was not found in Wooting Profiles database.";
  } else {
    Auth.social($routeParams.token);
    $location.path('/');
  }

});
