var app = angular.module('wootingProfilesRoutes', ['ngRoute'])

.config(function($routeProvider, $locationProvider) {

  $routeProvider.when('/', {
    templateUrl: 'app/views/pages/home.html',
    controller: 'codeController',
    controllerAs: 'code',
    // resolve: {
    //     games: function(Code) {
    //         return Code.getGames();
    //     }
    // }
  })
  .when('/games/:id', {
    templateUrl: 'app/views/pages/games/game.html',
    controller: 'gameCtrl',
    controllerAs: 'game'
  })
  .when('/register', {
    templateUrl: 'app/views/pages/users/register.html',
    controller: 'regCtrl',
    controllerAs: 'register',
    authenticated: false
  })
  .when('/login', {
    templateUrl: 'app/views/pages/users/login.html',
    authenticated: false
  })
  .when('/logout', {
    templateUrl: 'app/views/pages/users/logout.html',
    authenticated: true
  })
  .when('/profile', {
    templateUrl: 'app/views/pages/users/profile.html',
    authenticated: true
  })
  .when('/admin', {
    templateUrl: 'app/views/pages/admin/login.html',
    authenticated: false,
    // admin: true,
  })
  .when('/admin/dashboard', {
    templateUrl: 'app/views/pages/admin/dashboard.html',
    authenticated: true,
    admin: true,
    controller: 'codeListController',
    controllerAs: 'codelist',
    resolve: {
        games: function(Code) {
            return Code.getGames();
        }
    }
  })
  .when('/submitcode', {
    templateUrl: 'app/views/pages/codes/submitcode.html',
    controller: 'codeController',
    controllerAs: 'code'
  })
  .otherwise({
    redirectTo: '/'
  });

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

})
.service("Users", function($http, $routeParams) {
  this.getUsers = function() {
      return $http.get("/api/users").
          then(function(response) {
              return response;
          }, function(response) {
              alert("Error finding users.");
          });
  }

})

.controller("userListController", function(users, $scope) {
  $scope.users = users.data;
});

app.run(['$rootScope', 'Auth', '$location', function($rootScope, Auth, $location) {
  $rootScope.$on('$routeChangeStart', function(event, next, current) {
    // Check if user is authenticated
    if (next.$$route.authenticated == true) {
      if (!Auth.isLoggedIn()) {
        event.preventDefault();
        $location.path('/admin').search({ access: false });
      }
    } else if (next.$$route.authenticated  == false) {
      if (Auth.isLoggedIn()) {
        event.preventDefault();
        $location.path('/admin/dashboard');
      }
    };

  });

}]);

app.filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
});
