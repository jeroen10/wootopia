'use strict';

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    livereload = require('gulp-livereload'),
    cleanCSS = require('gulp-clean-css'),   
    htmlmin = require('gulp-htmlmin');

// Compile SASS
gulp.task('sass', () => {
    return gulp.src('public/assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/assets/sass/css'));
});

// Minify CSS
gulp.task('minify-css', () => {
    return gulp.src('public/assets/sass/css/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public/assets/css'));
});

// Minify HTML
gulp.task('minify-html', () => {
    return gulp.src('public/app/html/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('public/app/views'));
});

// Watch task
gulp.task('watch', () => {
    livereload.listen();
    gulp.watch('public/assets/sass/**/*.scss', ['sass', 'minify-css']);
    gulp.watch('public/app/html/**/*.html', ['minify-html']);    
});

gulp.task('default', ['watch']);